CC := g++ # This is the main compiler
# CC := clang --analyze # and comment out the linker last line for sanity
SRCDIR := src
BUILDDIR := build
#GREPTARGET1 := .*Visualiser.*
#TARGET1 := bin/parseMap
#GREPTARGET2 := .*Parser*
#TARGET2 := bin/visualise
TARGET := bin/mapVis
 
SRCEXT := cpp
#SOURCES1 := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT) | grep -v "$(GREPTARGET1)")
#SOURCES2 := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT) | grep -v "$(GREPTARGET2)")
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
#OBJECTS1 := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES1:.$(SRCEXT)=.o))
#OBJECTS2 := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES2:.$(SRCEXT)=.o))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
CFLAGS := -g -Wall
LIB := -L lib/pugixml-1.9/src/pugixml.cpp
INC := -I include

default: $(TARGET)

#$(TARGET1): $(OBJECTS1)
#	@echo " Linking..."
#	@echo " $(CC) $^ -o $(TARGET1) $(LIB)"; $(CC) $^ -o $(TARGET1) $(LIB)
#
#$(TARGET2): $(OBJECTS2)
#	@echo " Linking..."
#	@echo " $(CC) $^ -o $(TARGET2) $(LIB)"; $(CC) $^ -o $(TARGET2) $(LIB)
#
$(TARGET): $(OBJECTS)
	@echo " Linking..."
	@echo " $(CC) $^ -o $(TARGET) $(LIB)"; $(CC) $^ -o $(TARGET) $(LIB)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

clean:
	@echo " Cleaning..."; 
	@echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)
