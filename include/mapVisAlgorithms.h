#ifndef MAPVISALGORITHMS_H
#define MAPVISALGORITHMS_H
    #include <queue>
    #include "mapVisDataStructures.h"
    using namespace std;

    coordinate_list_element* search(graph_node *start_node, deque<graph_node*> node_container, bool queue);
    coordinate_list_element* breadth_first(graph_node *start_node);
    coordinate_list_element* depth_first(graph_node *start_node);
#endif
