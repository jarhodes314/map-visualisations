#ifndef MAPVISGRAPHS_H
#define MAPVISGRAPHS_H

#include <cstddef>

void join_nodes(graph_node *n1, graph_node *n2);

graph_node *pick_random_node(unordered_map<long, graph_node*> node_map);

#endif
