#ifndef MAPVISPARSER_H
#define MAPVISPARSER_H

bool is_number(const char *str);
int run_map_parser(int argc, char** argv);

#endif
