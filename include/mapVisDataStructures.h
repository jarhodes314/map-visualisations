#ifndef MAPVISDATASTRUCTURES_H
#define MAPVISDATASTRUCTURES_H

#include <cstddef>

struct coordinate
{
    double lat,lon;
};

struct graph_node : coordinate
{
    graph_node **connected_nodes = NULL;
    int number_connected = 0;
    int current_capacity = 0;
    bool visited = false;
};

struct coordinate_list_element : coordinate
{
    coordinate_list_element *next;
};

struct distance_node : graph_node
{
    /*
     * This is needed for Dijkstra's algorithm
     */
    double minimum_distance;
};

double haversine_distance(coordinate *node_from, coordinate *node_to);

#endif
