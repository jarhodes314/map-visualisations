#ifndef MAPVISCOLOURS_H
#define MAPVISCOLOURS_H

using namespace std;

namespace colour
{
    struct rgb
    {
        unsigned char R;
        unsigned char G;
        unsigned char B;
    };
    
    struct hsv
    {
        unsigned short H;
        double S;
        double V;
    };

    hsv *rgb_to_hsv(rgb *colour_rgb);

    rgb *hsv_to_rgb(hsv *colour_hsv);

    char *print(hsv *colour_hsv);

    char *print(rgb *colour_rgb);
}

#endif
