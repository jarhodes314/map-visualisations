#include <cstring>
#include "pugixml.hpp"
#include <iostream>
#include <unordered_map>
#include <stdlib.h>
#include <omp.h>
#include <unistd.h>
#include <fstream>
#include "mapVisDataStructures.h"
#include "mapVisAlgorithms.h"
#include "mapVisGraphs.h"

using namespace std;

bool debug = false;

static unordered_map<long, graph_node*> create_graph(char **argv)
{
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(argv[1]);

    if (!result)
    {
        cerr << "Error, could not parse XML file with name \"" << argv[1] << "\"" << endl;
        exit(-2);
    }

    unordered_map<long, graph_node*> node_map;

    for (pugi::xml_node dom_node = doc.child("osm").child("node"); dom_node; dom_node = dom_node.next_sibling())
    {
        if (!strcmp(dom_node.name(), "node"))
        {
            /*
             * Add a vertex to the hashmap
             */
            graph_node *vertex = (graph_node *) calloc(1,sizeof(graph_node));
            vertex->lat = atof(dom_node.attribute("lat").value());
            vertex->lon = atof(dom_node.attribute("lon").value());

            node_map.insert({atol(dom_node.attribute("id").value()), vertex});
        }
    }

    for (pugi::xml_node dom_node = doc.child("osm").child("way"); dom_node; dom_node = dom_node.next_sibling())
    {
        if (!strcmp(dom_node.name(), "way"))
        {
            /*
             * Connect vertices along a path
             */
            pugi::xml_node dom_nd1 = dom_node.child("nd");
            pugi::xml_node dom_nd2 = dom_nd1.next_sibling();
            while(dom_nd2)
            {
                auto nd1_search = node_map.find(atol(dom_nd1.attribute("ref").value()));
                auto nd2_search = node_map.find(atol(dom_nd2.attribute("ref").value()));

                if(nd1_search != node_map.end() && nd2_search != node_map.end())
                {
                    graph_node *nd1 = nd1_search->second;
                    graph_node *nd2 = nd2_search->second;
                    /*
                     * Add in the path in both directions
                     * TODO add support to consider mode of transport and one-way ness
                     */
                    join_nodes(nd1, nd2);
                    join_nodes(nd2, nd1);
                }
                dom_nd1 = dom_nd2;
                dom_nd2 = dom_nd2.next_sibling();
            }
        }
    }
    return node_map;
}

bool is_number(const char* s)
{
    int i;
    for (i = 0; s[i]; i++)
    {
        if (!isdigit(s[i]))
        {
            return false;
        }
    }
    if (i > 0) return true;
    else return false;
}

int run_map_parser(int argc, char** argv)
{
    int seed = time(NULL);
    srand(seed);
    if (!(argc > 1))
    {
        cerr << "Error, no input file specified" << endl;
        return -1;
    }

    unordered_map<long, graph_node*> node_map = create_graph(argv);

    graph_node *start_node = NULL; 
    for (int i = 2; i < argc; i++)
    {
        if (is_number(argv[i]))
        {
            long id = atol(argv[i]);
            if (node_map.count(id))
            {
                start_node = node_map.find(id) -> second;
                break;
            }
        }
    }

    start_node = start_node != NULL ? start_node : pick_random_node(node_map);
    coordinate_list_element *element = breadth_first(start_node);
    
    char *output_file_name;
    if (argc > 2)
    {
        output_file_name = argv[2];
    }
    else
    {
        char * extension = (char *) ".log";
        int extension_length = strlen(extension);
        int length = strlen(argv[1]);
        output_file_name = (char *) malloc((length + extension_length + 1) * sizeof(char));
        /*
         * Copy the input filename and the append the extension
         */
        memcpy(output_file_name, argv[1], length * sizeof(char));
        memcpy(output_file_name + length, extension, (extension_length + 1) * sizeof(char));
    }

    cout << "Writing log to " << output_file_name << endl;

    ofstream out(output_file_name);
    for(; element; element = element->next)
    {
        cout.precision(17);
        out << fixed << element->lat << " " << fixed << element->lon << " " << fixed << haversine_distance(start_node, element) << endl;
    }

    out.close();

    return 0;
}
