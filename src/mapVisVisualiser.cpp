#include "pugixml.hpp"
#include <iostream>
#include <unordered_map>
#include <cmath>
#include <cstring>
#include <stdlib.h>
#include <omp.h>
#include <unistd.h>
#include <fstream>
#include <vector>
#include "mapVisParser.h"
#include "mapVisDataStructures.h"
#include "mapVisColours.h"
#include <algorithm>

#define DEFAULT_RADIUS 15
#define EARTH_RADIUS 6371.0

using namespace std;
using namespace colour;

coordinate start_node;
double radius;

static void create_svg(char *log_name, char *svg_name, coordinate *min_coords, coordinate *max_coords, int point_count, int line_count)
{
    cout << "Creating svg at \"" << svg_name << "\"" << endl;
    ifstream log_file;
    log_file.open(log_name);
    ofstream svg_file;
    svg_file.open(svg_name);

    int width = sqrt(120 * point_count);
    int height = width;
    //int height = width * (max_y_norm - min_y_norm) / (max_x_norm - min_x_norm);
    
    rgb *colour_rgb;
    hsv *colour_hsv = (hsv *) malloc(sizeof(hsv));

    colour_hsv -> S = 1.0;
    colour_hsv -> V = 1.0;

      
    svg_file << "<svg version=\"1.1\"\n     baseProfile=\"full\"\n     width=\"" << width << "\" height=\"" << height << "\"\n     xmlns=\"http://www.w3.org/2000/svg\">\n";
    svg_file << "    <rect width=\"" << width << "\" height=\"" << height << "\" fill=\"black\" />\n";

    double lat;
    double lon;
    double distance;

    log_file >> start_node.lat;
    log_file >> start_node.lon;
    log_file >> distance;

    double delta_lon = asin(radius / (EARTH_RADIUS * cos(start_node.lat * M_PI / 180.0)));
    double delta_lat = asin(radius / EARTH_RADIUS);

    double delta_x = delta_lon / (2.0 * M_PI);
    double min_x_norm = start_node.lon / 360.0 - delta_x;
    //double delta_y = delta_lon / (2.0 * M_PI);
    double min_y_norm = (M_PI - log(tan(M_PI * 0.25 + (start_node.lat * M_PI / 180.0 + delta_lat) / 2.0))) / (2.0 * M_PI);
    double max_y_norm = (M_PI - log(tan(M_PI * 0.25 + (start_node.lat * M_PI / 180.0 - delta_lat) / 2.0))) / (2.0 * M_PI);
    
    double x_scaling = width / (2.0 * delta_x);
    double y_scaling = height / (max_y_norm - min_y_norm);

    int i = 1;

    for (int l = 1; i < point_count && l < line_count; l++)
    {

        log_file >> lat;
        log_file >> lon;
        log_file >> distance;

        if (distance < radius)
        {
            colour_hsv -> H = (360.0 * i) / ((double) point_count);
            colour_rgb = hsv_to_rgb(colour_hsv);

            double cx = (lon - 360.0 * min_x_norm) / 360.0 * x_scaling;
            double cy = ((M_PI - log(tan(M_PI * 0.25 + lat * M_PI / 360.0))) / (2 * M_PI) - min_y_norm)  * y_scaling;

            svg_file << "     <circle cx=\"" << cx << "\" cy=\"" << cy << "\" r=\"2\" fill=\"" << print(colour_rgb) << "\" />" << endl;
            i++;
        }
    }
    svg_file << "</svg>" << endl;
}

static void parse_log(char *log_name, char *svg_name)
{
    ifstream count_lines;
    count_lines.open(log_name);
    if (!count_lines.is_open())
    {
        cerr << "Error, could not open file with name \"" << log_name << "\"" << endl;
        exit(-1);
    }

    /*
     * From https://stackoverflow.com/a/3482093
     */
    
    // new lines will be skipped unless we stop it from happening:    
    count_lines.unsetf(ios_base::skipws);

    // count the newlines with an algorithm specialized for counting:
    unsigned line_count = count(
        istream_iterator<char>(count_lines),
        istream_iterator<char>(), 
        '\n');

    cout << "Reading " << line_count << " lines" << endl;

    count_lines.close();

    ifstream file;
    file.open(log_name);
    if (!file.is_open())
    {
        cerr << "Error, could not open file with name \"" << log_name << "\"" << endl;
        exit(-1);
    }

    coordinate *min_coords = (coordinate *) malloc(sizeof(coordinate));
    coordinate *max_coords = (coordinate *) malloc(sizeof(coordinate));
    min_coords -> lat = nan("1");
    min_coords -> lon = nan("1");
    max_coords -> lat = nan("1");
    max_coords -> lon = nan("1");

    int point_count = 0;

    for (unsigned int i = 0; i < line_count; i++)
    {
        double lat;
        file >> lat;

        if(file.eof())
        {
            break;
        }
        
        double lon;
        file >> lon;
        
        if(isnan(min_coords -> lat) || min_coords -> lat > lat)
        {
            min_coords -> lat = lat;
        }
        if(isnan(max_coords -> lat) || max_coords -> lat < lat)
        {
            max_coords -> lat = lat;
        }
        if(isnan(min_coords -> lon) || min_coords -> lon > lon)
        {
            min_coords -> lon = lon;
        }
        if(isnan(max_coords -> lon) || max_coords -> lon < lon)
        {
            max_coords -> lon = lon;
        }

        double distance;
        file >> distance;

        if (lon == lat && distance == lat)
        {
            cerr << "Error: is there a NaN in the logfile provided (\"" << log_name << "\")?" << endl;
            break;
        }
        
        if (distance < radius)
        {
            point_count++;
        }
    }

    cout << "Plotting from ";
    cout << "(" << min_coords -> lat << ", " << min_coords -> lon << ") to ";
    cout << "(" << max_coords -> lat << ", " << max_coords -> lon << ")" << "\n";
    cout << point_count << " nodes to plot" << endl;

    file.close();

    return create_svg(log_name, svg_name, min_coords, max_coords, point_count, line_count);
}

int visualise_from_log(int argc, char** argv)
{
    radius = argc > 2 && is_number(argv[2]) ? stoi(argv[2]) : DEFAULT_RADIUS;

    if (!(argc > 1))
    {
        cerr << "Error, no log file specified" << endl;
        return -1;
    }
    
    char* log_name = argv[1];
    cout << "Reading log from " << log_name << "\n" << endl;

    char* svg_name;
    if (argc > 2)
    {
        svg_name = argv[2];
    }
    else
    {
        int log_name_len = strlen(log_name);
        char *extension =  (char *) ".svg";
        svg_name = (char*) malloc(sizeof(char) * (log_name_len + 5));
        memcpy(svg_name, log_name, log_name_len);
        memcpy(svg_name + log_name_len, extension, strlen(extension) + 1);
    }

    parse_log(log_name, svg_name);

    return 0;
}
