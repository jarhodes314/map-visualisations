#include "mapVisColours.h"
#include <algorithm>
#include <cstdio>
#include <cmath>

#include <iostream>

using namespace std;

namespace colour
{
    hsv *rgb_to_hsv(rgb *colour_rgb)
    {
        hsv *colour_hsv = (hsv *) malloc(sizeof(hsv));

        const double R_ = colour_rgb -> R / 255.0;
        const double G_ = colour_rgb -> G / 255.0;
        const double B_ = colour_rgb -> B / 255.0;
        const double Cmax = max(max(R_,G_),B_);
        const double Cmin = min(min(R_,G_),B_);
        const double delta = Cmax - Cmin;
        
        colour_hsv -> S = Cmax ? delta / Cmax : 0;
        colour_hsv -> V = Cmax;
        if (delta)
        {
            if (Cmax == R_)
            {
                colour_hsv -> H = 60 * ((G_ - B_) / delta);
            }
            else if (Cmax == G_)
            {
                colour_hsv -> H = 60 * (((B_ - R_) / delta) + 2);
            }
            else
            {
                colour_hsv -> H = 60 * (((R_ - G_) / delta) + 4);
            }
            
            return colour_hsv;
        }
        else
        {
            colour_hsv -> H = 0;
            return colour_hsv;
        }
    }
    
    rgb *hsv_to_rgb(hsv *colour_hsv)
    {
        rgb *colour_rgb = (rgb *) malloc(sizeof(rgb));

        const double H_ = (double) (colour_hsv -> H) / 60.0;
        const double chroma = colour_hsv -> S * colour_hsv -> V;
        const double X = chroma * (1.0 - abs(fmod(H_, 2.0) - 1.0));
        const double m = (colour_hsv -> V - chroma) * 255;

        if (H_ < 1.0)
        {
            colour_rgb -> G = (X + m) * 255;
            colour_rgb -> R = (chroma + m) * 255;
            colour_rgb -> B = m * 255;
        }
        else if (H_ < 2.0)
        {
            colour_rgb -> R = (X + m) * 255;
            colour_rgb -> G = (chroma + m) * 255;
            colour_rgb -> B = m * 255;
        }
        else if (H_ < 3.0)
        {
            colour_rgb -> B = (X + m) * 255;
            colour_rgb -> G = (chroma + m) * 255;
            colour_rgb -> R = m * 255;
        }
        else if (H_ < 4.0)
        {
            colour_rgb -> G = (X + m) * 255;
            colour_rgb -> B = (chroma + m) * 255;
            colour_rgb -> R = m * 255;
        }
        else if (H_ < 5.0)
        {
            colour_rgb -> R = (X + m) * 255;
            colour_rgb -> B = (chroma + m) * 255;
            colour_rgb -> G = m * 255;
        }
        else if (H_ < 6.0)
        {
            colour_rgb -> B = (X + m) * 255;
            colour_rgb -> R = (chroma + m) * 255;
            colour_rgb -> G = m * 255;
        }

        return colour_rgb;
    }

    char* print(hsv *colour_hsv)
    {
        char *buffer = (char *) calloc(50, sizeof(char));
        sprintf(buffer, "(%i,%f,%f)", colour_hsv -> H, colour_hsv -> S, colour_hsv -> V);
        return buffer;
    }
    
    char* print(rgb *colour_rgb)
    {
        char *buffer = (char *) calloc(50, sizeof(char));
        sprintf(buffer, "#%02x%02x%02x", colour_rgb -> R, colour_rgb -> G, colour_rgb -> B);
        return buffer;
    }
}
