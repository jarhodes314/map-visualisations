#include "mapVisDataStructures.h"
#include <cstring>
#include <cstdlib>
#include <unordered_map>
#include <ostream>
#include <iostream>

using namespace std;

void join_nodes(graph_node *node_from, graph_node *node_to)
{
    int capacity = node_from->current_capacity;

    if(capacity <= node_from->number_connected)
    {
        /*
         * Allocated memory to double the size of the array and fill it by copying
         * original node pointers
         */
        if(!capacity)
        {
            node_from->connected_nodes = (graph_node**) calloc(2, sizeof(graph_node**));
            node_from->current_capacity = 2;
        }
        else
        {
            graph_node **original_connections = node_from->connected_nodes;
            node_from->connected_nodes = (graph_node**) calloc(capacity * 2, sizeof(graph_node**));
            node_from->current_capacity *= 2;
            memcpy(node_from->connected_nodes, original_connections, node_from->number_connected * sizeof(graph_node*));
            free(original_connections);
        }

        return join_nodes(node_from, node_to);
    }
    else
    {
        node_from->connected_nodes[node_from->number_connected] = node_to;
        node_from->number_connected++;
    }
}

graph_node *pick_random_node(unordered_map<long, graph_node*> node_map)
{
    auto iterator = node_map.begin();
    iterator = next(iterator, rand() % node_map.size());
    return iterator->second;
}
