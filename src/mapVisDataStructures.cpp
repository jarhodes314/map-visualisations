#include <cstddef>
#include "mapVisDataStructures.h"
#include <cmath>

#define EARTH_RADIUS_IN_KM 6371.0
#define TO_RAD (3.1415926536 / 180.0)

using namespace std;

/*
 * Compute the distance between two nodes in the real world, using the Haversine formula
 * The resulting value is in km
 */
double haversine_distance(coordinate *node_from, coordinate *node_to)
{
    double lat_difference = node_to -> lat - node_from -> lat;
    double lon_difference = node_to -> lon - node_from -> lon;

    double sine_lat = sin(lat_difference * 0.5 * TO_RAD);
    double sine_lon = sin(lon_difference * 0.5 * TO_RAD);
    double cos1 = cos(node_from -> lat * TO_RAD);
    double cos2 = cos(node_to   -> lat * TO_RAD);

    double a = pow(sine_lat,2.0) + cos1 * cos2 * pow(sine_lon,2.0);
    double c = 2.0 * asin(sqrt(a));

    return EARTH_RADIUS_IN_KM * c;
}
