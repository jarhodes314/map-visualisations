#include <queue>
#include <cstdlib>
#include "mapVisDataStructures.h"

using namespace std;

coordinate_list_element* search(graph_node *start_node, deque<graph_node*> node_container, bool queue)
{
    coordinate_list_element *search_order = (coordinate_list_element*)malloc(sizeof(coordinate_list_element));
    search_order->lat = start_node->lat;
    search_order->lon = start_node->lon;
    search_order->next = NULL;
    if (queue)
    {
        node_container.push_back(start_node);
    }
    else
    {
        node_container.push_front(start_node);
    }

    coordinate_list_element *current_list_element = search_order;

    while(!node_container.empty())
    {
        graph_node *current_node = node_container.front();
        node_container.pop_front();
        if (current_node->visited) 
        {
            continue;
        }
        current_node->visited = true;
        for(int i = 0; i < current_node->number_connected; i++)
        {
            graph_node *connected_node = current_node->connected_nodes[i];
            if (queue)
            {
                node_container.push_back(connected_node);
            }
            else
            {
                node_container.push_front(connected_node);
            }
        }

        coordinate_list_element *next_list_element = (coordinate_list_element*)malloc(sizeof(coordinate_list_element));
        next_list_element->lat = current_node->lat;
        next_list_element->lon = current_node->lon;
        next_list_element->next = NULL;
        current_list_element->next = next_list_element;
        current_list_element = current_list_element->next;
    }

    return search_order;
}

coordinate_list_element* breadth_first(graph_node *start_node)
{
    deque<graph_node*> node_queue;
    return search(start_node, node_queue, true);
}

coordinate_list_element* depth_first(graph_node *start_node)
{
    deque<graph_node*> node_stack;
    return search(start_node, node_stack, false);
}
