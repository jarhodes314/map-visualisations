#include "mapVisParser.h"
#include "mapVisVisualiser.h"
#include <cstring>
#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        int len = strlen(argv[1]);
        if (len >= 4 && !strcmp(&argv[1][len - 4], ".log"))
        {
            return visualise_from_log(argc, argv);
        }
        else
        {
            return run_map_parser(argc, argv);
        }
    }
    else
    {
        cout << "Error - no input file specified\n";
        cout << "Either pass an xml file or a logfile as an argument" << endl;
        return 0;
    }
}
