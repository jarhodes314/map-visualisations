# mapVis

This is a project, initially created in python, that allows the visualisation of the progressions of traversal algorithms (e.g. breadth-first, depth-first and Dijkstra's) through OpenStreetMap data. Due to poor performance of the original version, notably with the graph generation and visualisation rendering, the project is being re-written in C++. It will (hopefully) use OpenMP to help with the production of images and the underlying graph representation. 

## Installation and usage
This at the moment is pretty simple. Clone the repository and run make. You can then run `./stripScript.sh <FILENAME>` (which will remove some unnecessary attributes from the xml tree using perl) and then `./parseMap <FILENAME> <LOGFILENAME>` which currently performs breadth-first search and then outputs a log of the traversal in the form `latitude longitude distance_from_start`. 

## Examples
London, 25km radius, breadth-first search from Euston Road:
![London BFS](rendered/LondonSmall.jpg)
